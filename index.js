let firstName = 'John';
console.log('First Name: '+firstName);
let lastName = 'Smith';
console.log('Last Name: '+lastName);
let hisAge = 30;
console.log('Age: '+hisAge);
let hobbies = ['Biking','Mountain Climbing','Swimming'];
let work = {houseNumber: '32', street: 'Washington', city: 'Lincoln', state: 'Nebraska'};
console.log('Hobbies: ');
console.log(hobbies);
console.log('Work Address:');
console.log(work);
let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce",'Thor','Natasha',"Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullName2 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName2);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);